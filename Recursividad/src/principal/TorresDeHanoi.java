package principal;

public class TorresDeHanoi {
	
	public static void moverDisco (int numeroDeDiscos, int torreOrigen, int torreAuxiliar, int torreDestino) {
		
		if (numeroDeDiscos == 1) {
			
			System.out.println("Moviendo disco de " + torreOrigen + " a " + torreDestino);
		
		} else {
			
			moverDisco (numeroDeDiscos - 1, torreOrigen, torreDestino, torreAuxiliar);
			moverDisco (1, torreOrigen, torreAuxiliar, torreDestino);
			moverDisco (numeroDeDiscos - 1, torreAuxiliar, torreOrigen, torreDestino);
		}
		
	}
	
	
	public static void main (String [] args) {
		
		moverDisco (3, 1 ,2 ,3);
	}

}
