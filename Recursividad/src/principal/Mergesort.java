package principal;


public class Mergesort {
	
	public static int [] mergesort (int [] listaAOrdenar) {
		
		if (listaAOrdenar.length<=1) {
			
			return listaAOrdenar;
		}
		
		int medio = (listaAOrdenar.length)/2;
		int [] izquierdo = new int [medio];
		int [] derecho = new int [listaAOrdenar.length-medio];
		
		for (int i = 0; i < medio; i++) {
			
			izquierdo [i] = listaAOrdenar [i];
		}
		
		for (int j = medio; j < listaAOrdenar.length; j++) {
			
			derecho [j-medio] = listaAOrdenar [j];
		}
		
		int [] izqAIntercalar = mergesort (izquierdo);
		int [] derAIntercalar = mergesort (derecho);
		int [] resultado = intercalarOrdenado (izqAIntercalar, derAIntercalar);
		
		return resultado;
	
		
	}
	
	public static int [] intercalarOrdenado (int [] subArreglo1, int [] subArreglo2) {
		
		int i= 0;
		int j = 0;
		int k = 0;
		int [] listaResultado = new int [subArreglo1.length + subArreglo2.length];
		
		while (i < subArreglo1.length && j < subArreglo2.length) {
			
			if (subArreglo1[i] < subArreglo2[j]) {
				
				listaResultado [k] = subArreglo1[i];
				i++;
				
			} else {
				
				listaResultado [k] = subArreglo2[j];
				j++;
			}
			
			k++;
		}
		
		
		
		while (i < subArreglo1.length) {
			
			listaResultado [k] = subArreglo1[i];
			i++;
			k++;
		}
		
		while (j < subArreglo2.length) {
			
			listaResultado [k] = subArreglo2 [j];
			j++;
			k++;
		
		}
		
		return listaResultado;
		
	
	}
		
	public static void main (String [] args) {
		
		int [] array = {1,8,5,2,89,34,21};
		
		int [] ordenado = mergesort (array);
		
		for (int i = 0; i < ordenado.length; i++) {
			
			System.out.println(ordenado[i]);
		}
		
		
	}
}
